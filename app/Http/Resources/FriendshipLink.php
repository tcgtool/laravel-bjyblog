<?php

declare(strict_types=1);

namespace App\Http\Resources;

/**
 * @mixin \App\Models\FriendshipLink
 */
class FriendshipLink extends Base
{
}
